/**
 * Created by bisho on 29/04/2017.
 */
define([],function () {
    var data = [
         {
            name: "Vegetation",
            description: "",
            data: [
                {
                    index: 0,
                    timestamp: "February 2017",
                    url: "../images/vegetation/Feb_2017.PNG"
                },
                {
                    index: 1,
                    timestamp: "January 2017",
                    url: "../images/vegetation/Jan_2017.PNG"
                },
                {
                    index: 3,
                    timestamp: "December 2016",
                    url: "../images/vegetation/Dec_2016.PNG"
                },
                {
                    index: 4,
                    timestamp: "November2016",
                    url: "../images/vegetation/Nov_2016.PNG"
                },
                {
                    index: 5,
                    timestamp: "October 2016",
                    url: "../images/vegetation/Oct_2016.PNG"
                },
                {
                    index: 6,
                    timestamp: "September 2016",
                    url: "../images/vegetation/Sep_2016.PNG"
                },
                {
                    index: 7,
                    timestamp: "August 2016",
                    url: "../images/vegetation/Aug_2016.PNG"
                },
                {
                    index: 8,
                    timestamp: "July 2016",
                    url: "../images/vegetation/Jul_2016.PNG"
                },
                {
                    index: 9,
                    timestamp: "June 2016",
                    url: "../images/vegetation/Jun_2016.PNG"
                },
                {
                    index: 10,
                    timestamp: "May 2016",
                    url: "../images/vegetation/May_2016.PNG"
                },
                {
                    index: 11,
                    timestamp: "April 2016",
                    url: "../images/vegetation/Apr_2016.PNG"
                },
                {
                    index: 12,
                    timestamp: "March 2016",
                    url: "../images/vegetation/Mar_2016.PNG"
                },
                {
                    index: 13,
                    timestamp: "February 2016",
                    url: "../images/vegetation/Feb_2016.PNG"
                }
            ]
        },
        {
            name: "Land Surface Temperature Anomaly",
            description: "",
            data: [

                {
                    index: 0,
                    timestamp: "December 2016",
                    url: "../images/lst_anom/Dec_2016.PNG"
                },
                {
                    index: 1,
                    timestamp: "November2016",
                    url: "../images/lst_anom/Nov_2016.PNG"
                },
                {
                    index: 2,
                    timestamp: "October 2016",
                    url: "../images/lst_anom/Oct_2016.PNG"
                },
                {
                    index: 3,
                    timestamp: "September 2016",
                    url: "../images/lst_anom/Sep_2016.PNG"
                },
                {
                    index: 4,
                    timestamp: "August 2016",
                    url: "../images/lst_anom/Aug_2016.PNG"
                },
                {
                    index: 5,
                    timestamp: "July 2016",
                    url: "../images/lst_anom/Jul_2016.PNG"
                },
                {
                    index: 6,
                    timestamp: "June 2016",
                    url: "../images/lst_anom/Jun_2016.PNG"
                },
                {
                    index: 7,
                    timestamp: "May 2016",
                    url: "../images/lst_anom/May_2016.PNG"
                },
                {
                    index: 8,
                    timestamp: "April 2016",
                    url: "../images/lst_anom/Apr_2016.PNG"
                },
                {
                    index: 9,
                    timestamp: "March 2016",
                    url: "../images/lst_anom/Mar_2016.PNG"
                },
                {
                    index: 10,
                    timestamp: "February 2016",
                    url: "../images/lst_anom/Feb_2016.PNG"
                },
                {
                    index: 11,
                    timestamp: "January 2016",
                    url: "../images/lst_anom/Jan_2016.PNG"
                }
            ]
        },
        {
            name: "Land Surface Temperature",
            description: "",
            data:[
                {
                    index: 0,
                    timestamp: "February 2017",
                    url: "../images/lst/Feb_2017.PNG"
                },
                {
                    index: 1,
                    timestamp: "January 2017",
                    url: "../images/lst/Jan_2017.PNG"
                },
                {
                    index: 3,
                    timestamp: "December 2016",
                    url: "../images/lst/Dec_2016.PNG"
                },
                {
                    index: 4,
                    timestamp: "November2016",
                    url: "../images/lst/Nov_2016.PNG"
                },
                {
                    index: 5,
                    timestamp: "October 2016",
                    url: "../images/lst/Oct_2016.PNG"
                },
                {
                    index: 6,
                    timestamp: "September 2016",
                    url: "../images/lst/Sep_2016.PNG"
                },
                {
                    index: 7,
                    timestamp: "August 2016",
                    url: "../images/lst/Aug_2016.PNG"
                },
                {
                    index: 8,
                    timestamp: "July 2016",
                    url: "../images/lst/Jul_2016.PNG"
                },
                {
                    index: 9,
                    timestamp: "June 2016",
                    url: "../images/lst/Jun_2016.PNG"
                },
                {
                    index: 10,
                    timestamp: "May 2016",
                    url: "../images/lst/May_2016.PNG"
                },
                {
                    index: 11,
                    timestamp: "April 2016",
                    url: "../images/lst/Apr_2016.PNG"
                },
                {
                    index: 12,
                    timestamp: "March 2016",
                    url: "../images/lst/Mar_2016.PNG"
                },
                {
                    index: 13,
                    timestamp: "February 2016",
                    url: "../images/lst/Feb_2016.PNG"
                }
            ]
        }
];
    var DataSets = function() {};
    DataSets.data= data;
    /**
     * Function to get a specific image of a specific category
     * @param index
     * @param indexOne
     * @return {{}}
     */
    DataSets.getDataSet = function (index,indexOne) {
        var obj = {};
        console.log(data);
        console.log(index + " " + indexOne)
        obj.url= data[index].data[indexOne].url;
        obj.timestamp = data[i1].data[i2].timestamp;

        return obj;
    };

    return DataSets;
});

