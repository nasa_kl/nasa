/*
 * Copyright (C) 2014 United States Government as represented by the Administrator of the
 * National Aeronautics and Space Administration. All Rights Reserved.
 */
/**
 * Illustrates how to display and pick SurfaceImages.
 *
 * @version $Id: SurfaceImage.js 3320 2015-07-15 20:53:05Z dcollins $
 */

requirejs(['../src/WorldWind',
        './LayerManager', '../src/geom/Sector',
    '../js/DataSets','../lib/jquery.min','../node_modules/bootstrap-slider/dist/bootstrap-slider.min'],
    function (ww,
              LayerManager, Sector,DataSets,jq,slider) {
        "use strict";
    
        // Tell World Wind to log only warnings and errors.
        WorldWind.Logger.setLoggingLevel(WorldWind.Logger.LEVEL_WARNING);

        // Create the World Window.
        var wwd = new WorldWind.WorldWindow("canvasOne");

        /**
         * Added imagery layers.
         */
        var layers = [
            {layer: new WorldWind.BMNGLayer(), enabled: true},
            {layer: new WorldWind.BMNGLandsatLayer(), enabled: false},
            {layer: new WorldWind.CompassLayer(), enabled: true},
            {layer: new WorldWind.CoordinatesDisplayLayer(wwd), enabled: true},
            {layer: new WorldWind.ViewControlsLayer(wwd), enabled: true}
        ];

        for (var l = 0; l < layers.length; l++) {
            layers[l].layer.enabled = layers[l].enabled;
            wwd.addLayer(layers[l].layer);
        }


        var canvas = document.createElement("canvas"),
            ctx2d = canvas.getContext("2d"),
            size = 64, c = size / 2  - 0.5, innerRadius = 5, outerRadius = 20;

        canvas.width = size;
        canvas.height = size;

        var gradient = ctx2d.createRadialGradient(c, c, innerRadius, c, c, outerRadius);
        gradient.addColorStop(0, 'rgb(255, 0, 0)');
        gradient.addColorStop(0.5, 'rgb(0, 255, 0)');
        gradient.addColorStop(1, 'rgb(255, 0, 0)');

        ctx2d.fillStyle = gradient;
        ctx2d.arc(c, c, outerRadius, 0, 2 * Math.PI, false);
        ctx2d.fill();

        var addSurfaceImage= function (layer,isEnabled,index) {
            var surfaceImageLayer = new WorldWind.RenderableLayer();
            surfaceImageLayer.displayName = layer.name;
            surfaceImageLayer.addRenderable(new WorldWind.SurfaceImage(Sector.FULL_SPHERE,layer.data[index].url));
            surfaceImageLayer.enabled = isEnabled;
            wwd.addLayer(surfaceImageLayer);
        };

        // Add the surface images to a layer and the layer to the World Window's layer list.
        var d = DataSets.data;
        for(var i=0; i<d.length; i++){
            var layer = d[i];
            addSurfaceImage(layer,false,0)
        }



        $("#timeSldr").on("slideStop", function(slideEvt) {
            var index = slideEvt.value;
            var layer = d[layerIndex];
            console.log("LAYER:"+ layerIndex);
            $("#sldText").text(layer.data[index].timestamp);
            addSurfaceImage(layer,true,index);
            console.log(index)
            wwd.redraw();
        });
        
        // Create a layer manager for controlling layer visibility.
        var layerManger = new LayerManager(wwd);

        // Now set up to handle picking.

        // The common pick-handling function.
        var handlePick = function (o) {
            // The input argument is either an Event or a TapRecognizer. Both have the same properties for determining
            // the mouse or tap location.
            var x = o.clientX,
                y = o.clientY;

            // Perform the pick. Must first convert from window coordinates to canvas coordinates, which are
            // relative to the upper left corner of the canvas rather than the upper left corner of the page.
            var pickList = wwd.pick(wwd.canvasCoordinates(x, y));

            if (pickList.objects.length > 0) {
                for (var p = 0; p < pickList.objects.length; p++) {
                    if (pickList.objects[p].userObject instanceof WorldWind.SurfaceImage) {
                        console.log("Surface image picked");
                    }
                }
            }
        };

        //pan to kl
        var gt = new WorldWind.GoToAnimator(wwd)
        gt.goTo(new WorldWind.Location(3.134848, 101.713554));

        // Listen for mouse moves and highlight the placemarks that the cursor rolls over.
        wwd.addEventListener("mousemove", handlePick);

        // Listen for taps on mobile devices and highlight the placemarks that the user taps.
        var tapRecognizer = new WorldWind.TapRecognizer(wwd, handlePick);
    });